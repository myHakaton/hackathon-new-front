export const categoryList = [
  {
    name:'Спорт',
    color:'#FFF9C4',
    icon:'mdi-label'
  },
  {
    name:'Здоровье',
    color: '#B3E5FC'
  },
  {
    name:'Культура',
    color: '#FFCDD2'
  },
  {
    name:'Образование',
    color: '#DCEDC8'
  },
  {
    name: 'Общество',
    color: '#FFE0B2'
  },
  {
    name: 'Работа',
    color: '#B2DFDB'
  },
  {
    name: 'Разное',
    color: '#E1BEE7'
  },
  {
    name: 'Технологии',
    color: '#D7CCC8'
  }
];
