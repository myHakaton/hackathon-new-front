module.exports = {
  transpileDependencies: ['vue-clamp', 'resize-detector'],
  devServer: {
    proxy: {
      '/api1': {
        target: 'http://192.168.120.130:5000/',
        pathRewrite: {
          '/api1': ''
        }
      },
      '/api2': {
        target: 'http://192.168.120.130:8080/',
        pathRewrite: {
          '/api2': ''
        }
      }
    }
  }
};
